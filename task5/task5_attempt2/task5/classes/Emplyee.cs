﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5.classes
{
    public class Emplyee : IEnumerable, IEnumerator, Emplyee.IEquatable, IComparable, IComparer
    {
        public string firstName;
        public string lastName;
        public string pesonalNumber;
        public DateTime birthday;
        public DateTime age;
        public int sallary;
        public int rating;


        #region 1
        public Emplyee() { }
        public Emplyee(string firstName, string lastName, string pesonalNumber, DateTime birthday, int sallary, byte rating)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.pesonalNumber = pesonalNumber;
            this.birthday = birthday;
            this.sallary = sallary;
            this.rating = rating;
        }
        #endregion


        int IComparable.CompareTo(object obj)
        {
            throw new NotImplementedException();
        }

        public int Compare(object x, object y)
        {
            throw new NotImplementedException();
        }

        bool System.Collections.IEnumerator.MoveNext()
        {
            throw new NotImplementedException();
        }

        void System.Collections.IEnumerator.Reset()
        {
            throw new NotImplementedException();
        }

        public System.Collections.IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
        object System.Collections.IEnumerator.Current => throw new NotImplementedException();

        public interface IEnumerable
        {
        }

        public interface IEquatable
        {
        }

    }

    public class SortByRaitingDescending : IComparer
    {
        int IComparer.Compare(object x, object y)
        {
            Emplyee emplyee = (Emplyee)x;
            Emplyee emplyee1 = (Emplyee)y;
            return String.Compare(emplyee.rating, emplyee.rating);
        }
    }

    public class SortBysallaryAscending : IComparer
    {




        int IComparer.Compare(object x, object y)
        {
            Emplyee emplyee = (Emplyee)x;
            Emplyee emplyee1 = (Emplyee)y;
            if (emplyee.sallary < emplyee1.sallary)
                return 1;

            if (emplyee.sallary > emplyee1.sallary)
                return -1;

            else
                return 0;
        }
    }
    public class SortByAgeAscending : IComparer
    {
        int IComparer.Compare(object x, object y)
        {
            Emplyee emplyee = (Emplyee)x;
            Emplyee emplyee1 = (Emplyee)y;
            if (emplyee.age < emplyee1.age)
                return 1;

            if (emplyee.age > emplyee1.age)
                return -1;

            else
                return 0;
        }
    }
}
