﻿namespace ConsoleApp1.classes
{
    public abstract class Geometry
    {
        public abstract int Perimeter();
    }

    public class Rectangle : Geometry
    {
        public int sideA { get; }
        public int sideB { get; }

        public Rectangle(int sideA, int sideB)
        {
            this.sideA = sideA;
            this.sideB = sideB;
        }

        public override int Perimeter()
        {
            return 2 * (sideA + sideB);
        }
    }

    public class Triangle : Geometry
    {
        public int side1 { get; }
        public int side2 { get; }
        public int side3 { get; }

        public Triangle(int side1, int side2, int side3)
        {
            this.side1 = side1;
            this.side2 = side2;
            this.side3 = side3;
        }

        public override int Perimeter()
        {
            return side1 + side2 + side3;
        }
    }
}
