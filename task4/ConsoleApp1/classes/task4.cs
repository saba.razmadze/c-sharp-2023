﻿namespace ConsoleApp1.classes
{
    public abstract class Method4
    {
        public abstract double YearlyPay();
    }

    public class Employee : Method4
    {
        private double MonthlySalary;

        public Employee(double monthlySalary)
        {
            this.MonthlySalary = monthlySalary;
        }

        public override double YearlyPay()
        {
            return MonthlySalary * 12;
        }
    }
}
