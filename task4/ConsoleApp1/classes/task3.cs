﻿namespace ConsoleApp1.classes
{
    public abstract class Method
    {
        public abstract double method();
    }

    public class ElectricityConsumption3 : Method
    {
        public double CpH { get; }
        public double Duration { get; }

        public ElectricityConsumption3(double cpH, double duration)
        {
            this.CpH = cpH;
            this.Duration = duration;
        }

        public override double method()
        {
            return CpH * Duration;
        }
    }
}
