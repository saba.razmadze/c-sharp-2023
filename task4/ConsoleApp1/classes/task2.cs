﻿namespace ConsoleApp1.classes
{
    public abstract class Train
    {
        public abstract double Calculate();
    }

    public class TrainDistance : Train
    {
        private double Speed;
        private double Duration;

        public TrainDistance(double speed, double duration)
        {
            this.Speed = speed;
            this.Duration = duration;
        }

        public override double Calculate()
        {
            return Speed * Duration;
        }
    }

    public class ElectricityConsumption : Train
    {
        private double KwH;
        private double Distance;

        public ElectricityConsumption(double kwH, double distance)
        {
            this.KwH = kwH;
            this.Distance = distance;
        }

        public override double Calculate()
        {
            return KwH * Distance;
        }
    }
}
