﻿using ConsoleApp1.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1.classes;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Rectangle rectangle = new Rectangle(5, 4);
            Triangle triangle = new Triangle(3,4,5);

            Console.WriteLine("task1 starts here\n");
            Console.WriteLine($"rectangle Perimeter  is: {rectangle.Perimeter()} cm");
            Console.WriteLine($"triangle Perimeter  is: {triangle.Perimeter()} cm");


            Console.WriteLine("\ntask2 starts here\n");
            TrainDistance distance = new TrainDistance(120, 4);
            ElectricityConsumption consumption = new ElectricityConsumption(150, 300);
            Console.WriteLine($"total distance is: {distance.Calculate()} km");
            Console.WriteLine($"total electricity is: {consumption.Calculate()} kwH");


            Console.WriteLine("\ntask3 starts here\n");
            ElectricityConsumption3 tv = new ElectricityConsumption3(80, 4);
            Console.WriteLine($"electricity of TV is : {tv.method()} Kw");


            Console.WriteLine("\ntask4 starts here\n");
            Employee employee = new Employee(4000);
            Console.WriteLine($"Salary is: {employee.YearlyPay()} lari");


            Console.ReadKey();
        }
    }
}
