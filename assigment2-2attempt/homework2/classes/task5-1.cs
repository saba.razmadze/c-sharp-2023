﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework2.classes
{
    public class plane
    {
        private int distance;
        private int fuelCopacity;
        public int peopleAmount;
        public int ticketsSold;
        
        public plane()
        {
            Console.WriteLine("plane has been created");
        }
        public plane(int distance, int fuelCopacity, int peopleAmount, int ticketsSold):this() 
        {
            this.distance= distance;
            this.fuelCopacity= fuelCopacity;
            this.peopleAmount= peopleAmount;
            this.ticketsSold= ticketsSold;
        }

    }
    public class students
    {
        private string firstName;
        private string lastName;
        private int age;
        public string univerity;
        public int course;

        public students()
        {
            Console.WriteLine("new student is added");
        }
        public students(string firstName, string lastName, int age) : this() { 
            this.firstName= firstName;
            this.lastName= lastName;
            this.age= age;  
        }
        public students(string firstName, string lastName, int age, string univerity, int course) : this(firstName, lastName, age)
        {
            this.univerity = univerity;
            this.course = course;
        }
    }
    public class train
    {
        private int sumOfVagons;
        private int passangers;
        public int ticketPrice;
        public int soldtickets;

    }
    public class ractangular
    {
        private float perimeter;
        private float area;
        public float side1;
        public float side2;
    }
    public class car
    {
        private string carcolour;
        private int numberOfDors;
        public string lastNameOfOwner;
        public string nameOfBrand;
    }

}
