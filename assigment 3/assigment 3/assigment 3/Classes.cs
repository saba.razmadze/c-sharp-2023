﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace assigment_3
{
    public class Triangular
    {
        protected float side1;
        protected float side2;
        protected float side3;

        public Triangular()
        {
            Console.WriteLine("new triangular is created");
        }
        public Triangular(float side1, float side2, float side3) :this()
        {
            this.side1 = side1;
            this.side2 = side2;
            this.side3 = side3;
        }

    }
    public class Triangular2 : Triangular
    {
        private float perimeter;
        public Triangular2(float side1, float side2, float side3)
        {
            this.side1 = side1;
            this.side2 = side2;
            this.side3 = side3;
        }
        public float Area()
        {
            float area = side1 * side2 * side3;
            return area;
        }

        public float Perimeter_method()
        {
            perimeter = side1 + side2 + side3;
            return perimeter;
        }

    }

    public class BasicRactangular
    {
        protected float ractangular_base;

        public BasicRactangular()
        {
           
        }
        public BasicRactangular(float ractangular_base)
        {
            this.ractangular_base = ractangular_base;
        }
    }
    public class Ractangular: BasicRactangular
    {
        protected float ractangular_height;

        public Ractangular(float ractangular_height, float ractangular_base)
        {
            this.ractangular_height = ractangular_height;
            this.ractangular_base=ractangular_base;
        }

        public float Area()
        {
            return ractangular_base*ractangular_height;
        }

    }

}
